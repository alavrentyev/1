
{include file="common/subheader.tpl" title=__("ccp") target="#changing_category_page_addon"}

<div id="changing_category_page_addon" class="in collapse">
    
    <div class="control-group">
        <label class="control-label" for="scn">{__("show_category_name")}:</label>
        <div class="controls">
        <input type="hidden" value="N" name="category_data[show_category_name]"/>
        <input type="checkbox" class="cm-toggle-checkbox" value="Y" name="category_data[show_category_name]" id="scn" {if $category_data.show_category_name == "Y"} checked="checked"{/if} />
        </div>
    </div>
    
     <div class="control-group">
        <label class="control-label" for="sb">{__("show_subcategories")}:</label>
        <div class="controls">
        <input type="hidden" value="N" name="category_data[show_subcategories]"/>
        <input type="checkbox" class="cm-toggle-checkbox" value="Y" name="category_data[show_subcategories]" id="sb" {if $category_data.show_subcategories == "Y"} checked="checked"{/if} />
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="elm_category_page_title">{__("page_title")}:</label>
        <div class="controls">
            <input type="text" name="category_data[page_title]" id="elm_category_page_title" size="55" value="{$category_data.page_title}" class="input-large" />
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="st">{__("show_title")}:</label>
        <div class="controls">
        <input type="hidden" value="N" name="category_data[show_title]"/>
        <input type="checkbox" class="cm-toggle-checkbox" value="Y" name="category_data[show_title]" id="st" {if $category_data.show_title == "Y"} checked="checked"{/if} />
        </div>
    </div>
</div>