<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'view') {

      $category_page = db_get_row("SELECT show_category_name, show_subcategories, show_title FROM ?:categories WHERE category_id = ?i", $_SESSION['current_category_id']);

	if ($category_page['show_category_name'] == 'N') {
		$category_data['category'] = '';
		Registry::get('view')->assign('category_data', $category_data['category']); 
	} 
    
	if ($category_page['show_subcategories'] == 'N') {
		$category_data['subcategories'] = '';
		Registry::get('view')->assign('subcategories', $category_data['subcategories']);
	}
    
	if ($category_page['show_title'] == 'N') {
		$category_data['page_title'] = '';
		Registry::get('view')->assign('page_title', $category_data['page_title']);
	} 
}
