<?php


use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'checkout') {

	$cart = Registry::get('view')->getTemplateVars('cart');
	$cart['user_data']['s_title'] = '';
	Tygh::$app['view']->assign('cart', $cart);   
}