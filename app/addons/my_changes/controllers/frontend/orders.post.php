<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'details') {

	$order_info = Registry::get('view')->getTemplateVars('order_info');
	$order_info['s_title'] = '';
	Tygh::$app['view']->assign('order_info', $order_info);
}

